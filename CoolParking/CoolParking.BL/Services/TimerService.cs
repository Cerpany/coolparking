﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : Interfaces.ITimerService
    {
        public double Interval { get; set; }

        private readonly Timer Timer;

        public event ElapsedEventHandler Elapsed;
        public void FireElapsedEvent(object obj, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(obj,e);
        }

        public void Dispose()
        {
            this.Stop();
        }

        public void Start()
        {
            Timer.Elapsed += Elapsed;
            Timer.Start();
            Timer.AutoReset = true;
            Timer.Enabled = true;
        }

        public void Stop()
        {
            Timer.Stop();
        }
        public TimerService(double interval)
        {
            Interval = interval;
            Timer = new Timer(interval);
        }

    }
}
