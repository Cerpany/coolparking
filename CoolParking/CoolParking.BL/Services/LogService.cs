﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : Interfaces.ILogService
    {
        public string LogPath { get; private set; }

        public string Read()
        {
            try
            {
                using FileStream fstream = File.OpenRead(LogPath);
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                return System.Text.Encoding.Default.GetString(array);
            }
            catch
            {
                throw new System.InvalidOperationException();
            }
        }


        public void Write(string logInfo)
        {
            using FileStream fstream = new(LogPath, FileMode.OpenOrCreate);
            byte[] array = System.Text.Encoding.Default.GetBytes(logInfo);
            fstream.Write(array, 0, array.Length);
        }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
    }
}
