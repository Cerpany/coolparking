﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using System.Timers;
using System.Collections.Generic;
using System;
using System.Linq;


namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService LogTimer;
        private readonly ITimerService WithdrawTimer;
        private readonly ILogService LogService;
        private readonly List<TransactionInfo> LastParkingTransactions;
        public void AddVehicle(Vehicle vehicle)
        {
            if (this.GetFreePlaces() <= 0)
            {
                throw new InvalidOperationException("Parking is full");
            }
            else if (Parking.GetParking().Vehicles.Find(x => x.Id == vehicle.Id) != null)
            {
                throw new ArgumentException("Vehicle already added");
            }
            else
            {
                Parking.GetParking().Vehicles.Add(vehicle);
            }
        }

        public void Dispose()
        {
            LogTimer.Dispose();
            WithdrawTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return GetLastParkingTransactions().Sum(x => x.Sum);
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.Capacity - Parking.GetParking().Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return LastParkingTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.GetParking().Vehicles);
        }

        public string ReadFromLog()
        {
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (Parking.GetParking().Vehicles.Find(x => x.Id == vehicleId) == null)
            {
                throw new ArgumentException("There is no such vehicle");
            }
            else
            {
                if (Parking.GetParking().Vehicles.Find(x => x.Id == vehicleId).Balance < 0)
                {
                    throw new InvalidOperationException("Pay a dept to remove this vehicle");
                }
                else
                {
                    Parking.GetParking().Vehicles.Remove(Parking.GetParking().Vehicles.Find(x => x.Id == vehicleId));
                }
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (Parking.GetParking().Vehicles.Find(x => x.Id == vehicleId) == null)
            {
                throw new ArgumentException("There is no such vehicle");
            }
            if (sum <= 0)
            {
                throw new ArgumentException("Сannot be top uped by a non-positive number");
            }
            else
            {
                Parking.GetParking().Vehicles.Find(x => x.Id == vehicleId).Balance += sum;
            }
        }
        private void TakePaying(object source, ElapsedEventArgs e)
        {

            foreach(Vehicle vehicle in GetVehicles())
            {
                LastParkingTransactions.Add(new TransactionInfo(DateTime.Now,vehicle.Id, vehicle.PayForParking()));      
            }
        }
        private void MakeLog(object source, ElapsedEventArgs e)
        {
            foreach (TransactionInfo transaction in GetLastParkingTransactions())
            {
                LogService.Write(transaction.ToString());
            }
            LastParkingTransactions.Clear();
        }
        public string GetAllTransactionInfo()
        {
            string newtransacts = "";
            foreach (TransactionInfo transaction in GetLastParkingTransactions())
            {
                newtransacts += transaction.ToString() + "\n";
            }
            return LogService.Read() + newtransacts;
        }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            LastParkingTransactions = new List<TransactionInfo>();
            WithdrawTimer = withdrawTimer;
            LogTimer = logTimer;
            LogService = logService;
            WithdrawTimer.Elapsed += new ElapsedEventHandler(TakePaying);
            LogTimer.Elapsed += new ElapsedEventHandler(MakeLog);
            withdrawTimer.Start();
            logTimer.Start();

        }
    }
}
