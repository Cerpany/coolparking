﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal StartBalance = 0;
        public const int Capacity = 10;
        public const double PaymentPeriodMilliseconds = 5000;
        public const double LogPeridMilliseconds = 60000;
        public static decimal Payment(VehicleType vehicleType)
        {
            return vehicleType switch
            {
                VehicleType.PassengerCar => 2,
                VehicleType.Truck => 5,
                VehicleType.Bus => 3.5M,
                VehicleType.Motorcycle => 1,
                _ => throw new System.NotImplementedException("Vehicle type is not supported"),
            };
        }
        public const decimal PenaltyCoeff = 2.5M;

    }
}