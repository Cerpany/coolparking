﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System.Collections.Generic;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly List<string> UsedIds = new();
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance <= 0 || !Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                throw new ArgumentException("Incorrect balance or id");
            }
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public decimal PayForParking()
        {
            decimal payment = Settings.Payment(VehicleType);
            Parking.GetParking().Balance += Settings.Payment(VehicleType);
            Balance -= Settings.Payment(VehicleType);
            if (Balance < 0)
            {
                payment += Settings.PenaltyCoeff * Settings.Payment(VehicleType);
                Parking.GetParking().Balance += Settings.PenaltyCoeff * Settings.Payment(VehicleType);
                Balance -= Settings.PenaltyCoeff * Settings.Payment(VehicleType);
            }
            return payment;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            String id;
            do
            {
                var plateNumber = new StringBuilder();
                Random rnd = new();
                plateNumber.Append(value: GenerateRandomTwoLetters());
                plateNumber.Append(value: '-');
                plateNumber.Append(value: rnd.Next(1000, 9999));
                plateNumber.Append(value: '-');
                plateNumber.Append(value: GenerateRandomTwoLetters());
                id = plateNumber.ToString();
            } while (UsedIds.Contains(id));
            return id;
        }
        private static string GenerateRandomTwoLetters()
        {
            var TwoLetters = new StringBuilder();
            Random rnd = new();
            char offset = 'A';
            int lettersOffset = 26;
            for (var i = 0; i < 2; i++)
            {
                var @char = (char)rnd.Next(offset, offset + lettersOffset);
                TwoLetters.Append(@char);
            }
            return TwoLetters.ToString();
        }
        public override string ToString()
        {
            return (Id + "\t" + VehicleType.ToString());
        }
    }
}