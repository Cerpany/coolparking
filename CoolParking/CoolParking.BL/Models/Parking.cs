﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking parking;
        public decimal Balance { get; internal set; }
        public List<Vehicle> Vehicles { get; private set; }
        private Parking()
        {
            Balance = Settings.StartBalance;
            Vehicles = new List<Vehicle>(Settings.Capacity);
        }
        public static Parking GetParking()
        {
            if (parking == null)
            {

                {
                    parking = new Parking();
                }
            }
            return parking;
        }
    }
}