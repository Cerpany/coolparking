﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime DateTime { get; set; } 
        public string Id { get; set; }
        public decimal Sum { get; set; }
        public TransactionInfo(DateTime dateTime, string id, decimal sum)
        {
            DateTime = dateTime;
            Id = id;
            Sum = sum;
        }
        public override string ToString()
        {
            return (DateTime.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + "\t" + Id + "\t" + Sum.ToString());
        }
    }
}

