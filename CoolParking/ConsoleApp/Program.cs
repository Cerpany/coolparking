﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using System.IO;
using System.Reflection;
using CoolParking.BL.Models;

namespace ConsoleApp
{
    class Program
    {
        static void Main()
        {
            LogService logService = new($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            TimerService withdrawTimer = new(Settings.PaymentPeriodMilliseconds);
            TimerService logTimer = new(Settings.LogPeridMilliseconds);
            ParkingService parking = new(withdrawTimer, logTimer, logService);
            Console.WriteLine("You are greeted by the coolest parking, select the action by entering its number: \n" +
            "1.Display the current balance of the Parking. \n" +
            "2.Display the amount of earned funds for the current period. \n" +
            "3.Display the number of free parking spaces. \n" +
            "4.Display all Parking Transactions for the current period. \n" +
            "5.Display the transaction history. \n" +
            "6.Display the list Vehicle  in the parking lot. \n" +
            "7.Put the vehicle in the parking lot. \n" +
            "8.Remove the vehicle from the Parking. \n" +
            "9.Top up the balance of a particular vehicle. \n" +
            "10.Close Program");
            bool work = true;
            do
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Parking balance is " + Parking.GetParking().Balance);
                        break;
                    case "2":
                        Console.WriteLine("Current balance is "+parking.GetBalance());
                        break;
                    case "3":
                        Console.WriteLine("Number of free parking spaces is " + parking.GetFreePlaces() + " from " + parking.GetCapacity());
                        break;
                    case "4":
                        foreach (TransactionInfo transaction in parking.GetLastParkingTransactions())
                        {
                            Console.WriteLine(transaction.ToString());                           
                        }
                        Console.WriteLine("Enter next command");
                        break;
                    case "5":

                        Console.WriteLine(parking.GetAllTransactionInfo());
                        Console.WriteLine("Enter next command");
                        break;
                    case "6":
                        Console.WriteLine("Vehicles on parking:");
                        foreach (Vehicle vehicle in parking.GetVehicles())
                        {
                            Console.WriteLine(vehicle.ToString());
                        }
                        Console.WriteLine("Enter next command");
                        break;
                    case "7":

                        try
                        {
                            Console.WriteLine("Enter vehicle id in format AA-0000-AA:");
                            string id = Console.ReadLine();
                            Console.WriteLine("Enter vehicle type PassengerCar, Truck, Bus, Motorcycle (please use full form):");
                            var vehicleType = Console.ReadLine() switch
                            {
                                "PassengerCar" => VehicleType.PassengerCar,
                                "Truck" => VehicleType.Truck,
                                "Bus" => VehicleType.Bus,
                                "Motorcycle" => VehicleType.Motorcycle,
                                _ => throw new ArgumentException("Incorect vehicle type"),
                            };
                            Console.WriteLine("Enter vehicle balance:");
                            decimal balance = decimal.Parse(Console.ReadLine());
                            parking.AddVehicle(new(id, vehicleType, balance));
                            Console.WriteLine("Vehicles was succesfully added");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message + ", vehicle cannot be added");
                        }
                        break;
                    case "8":
                        try
                        {
                            Console.WriteLine("Enter vehicle id:");
                            parking.RemoveVehicle(Console.ReadLine());
                            Console.WriteLine("Vehicles was succesfully removed");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "9":
                        try
                        {
                            Console.WriteLine("Enter vehicle id:");
                            string id = Console.ReadLine();
                            Console.WriteLine("Enter the amaunt:");
                            decimal sum = decimal.Parse(Console.ReadLine());
                            parking.TopUpVehicle(id, sum);
                            Console.WriteLine("Vehicles was succesfully top uped");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }             
                        break;
                    case "10":
                        work = false;
                        break;
                    default:
                        Console.WriteLine("Incorrect input");
                        break;
                }
            } while (work);
        }
    }

}
